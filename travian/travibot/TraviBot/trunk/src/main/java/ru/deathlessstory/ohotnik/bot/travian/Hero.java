package ru.deathlessstory.ohotnik.bot.travian;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 07.12.13
 * Time: 12:41
 */
public class Hero {

    private String name;
    private int health;
    private int exp;
    private String lvl;
    private String status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public String getLvl() {
        return lvl;
    }

    public void setLvl(String lvl) {
        this.lvl = lvl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setReso(String id) {
        WebDriver driver = Driver.getInstance();
        WebElement element = driver.findElement(By.id("heroImageButton"));
        element.click();
        element = driver.findElement(By.xpath("//img[@alt='Параметры героя']"));
        element.click();
        element = driver.findElement(By.id("resourceHero" + id));
        element.click();
        element = driver.findElement(By.id("saveHeroAttributes"));
        element.click();
    }
}
