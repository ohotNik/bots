package ru.deathlessstory.ohotnik.bot.travian;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 14.12.13
 * Time: 0:03
 */
public class CityInfo {

    private int wood;
    private int claim;
    private int stone;
    private int eat;

    public int getWood() {
        return wood;
    }

    public void setWood(int wood) {
        this.wood = wood;
    }

    public int getClaim() {
        return claim;
    }

    public void setClaim(int claim) {
        this.claim = claim;
    }

    public int getStone() {
        return stone;
    }

    public void setStone(int stone) {
        this.stone = stone;
    }

    public int getEat() {
        return eat;
    }

    public void setEat(int eat) {
        this.eat = eat;
    }

    public synchronized void update() {
        WebDriver driver = Driver.getInstance();
        WebElement element = driver.findElement(By.id("l1"));
        wood = Integer.parseInt(element.getText().trim().replaceAll("\\.", ""));
        element = driver.findElement(By.id("l2"));
        claim = Integer.parseInt(element.getText().trim().replaceAll("\\.", ""));
        element = driver.findElement(By.id("l3"));
        stone = Integer.parseInt(element.getText().trim().replaceAll("\\.", ""));
        element = driver.findElement(By.id("l4"));
        eat = Integer.parseInt(element.getText().trim().replaceAll("\\.", ""));
    }
}
