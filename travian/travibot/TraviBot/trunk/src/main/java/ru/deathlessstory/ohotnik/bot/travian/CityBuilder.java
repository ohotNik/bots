package ru.deathlessstory.ohotnik.bot.travian;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 14.12.13
 * Time: 0:05
 */
public class CityBuilder {

    private static boolean flag;

    public synchronized void stop() {
        flag = false;
    }

    public void start() {
        flag = true;
        OutThread outThread = new OutThread();
        outThread.start();
    }

    class OutThread extends Thread {
        @Override
        public void run() {
            while (flag) {
                iter();
                try {
                    Thread.sleep(600000 + (new Random().nextInt(10000)));
                } catch (InterruptedException e) {
                    flag = false;
                    e.printStackTrace();
                }
            }
        }
    }

    private synchronized void iter() {
        WebDriver driver = Driver.getInstance();
        WebElement webElement = driver.findElement(By.id("n1"));
        webElement = webElement.findElement(By.tagName("a"));
        webElement.click();
        WebDriverWait wait = new WebDriverWait(driver, 120);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("village_map")));
        webElement = driver.findElement(By.id("village_map"));
        List<WebElement> list = webElement.findElements(By.tagName("div"));
        int min = 100;
        int pos = 1000;
        int i = 0;
        for (WebElement element : list) {
            int lvl = Integer.parseInt(element.getText().trim());
            if (lvl < min) {
                min = lvl;
                pos = i;
            }
            i++;

        }

        webElement = driver.findElement(By.id("rx"));
        List<WebElement> areas = webElement.findElements(By.tagName("area"));
        webElement = areas.get(pos);
        webElement.click();

        findNeedRes();
    }

    public void findNeedRes() {
        CityInfo cityInfo = new CityInfo();
        cityInfo.update();
        Hero hero = new Hero();
        WebDriver driver = Driver.getInstance();
        WebElement contract = driver.findElement(By.id("contract"));
        int r1 = Integer.parseInt(contract.findElement(By.className("r1")).getText().trim());
        int r2 = Integer.parseInt(contract.findElement(By.className("r2")).getText().trim());
        int r3 = Integer.parseInt(contract.findElement(By.className("r3")).getText().trim());
        int r4 = Integer.parseInt(contract.findElement(By.className("r4")).getText().trim());
        int delta1 = r1 - cityInfo.getWood();
        int delta2 = r2 - cityInfo.getClaim();
        int delta3 = r3 - cityInfo.getStone();
        int delta4 = r4 - cityInfo.getEat();
        int max = Math.max(Math.max(delta1, delta2), Math.max(delta3, delta4));

        if (max < 0) {
            build();
            return;
        }

        Date date = new Date();

        if (delta1 == max) {
            System.out.println(date + " Не хватает дерева (" + delta1 + ")");
            hero.setReso("1");
            return;
        }
        if (delta2 == max) {
            System.out.println(date + " Не хватает глины (" + delta2 + ")");
            hero.setReso("2");
            return;
        }
        if (delta3 == max) {
            System.out.println(date + " Не хватает камня (" + delta3 + ")");
            hero.setReso("3");
            return;
        }
        if (delta4 == max) {
            System.out.println(date + " Не хватает еды (" + delta4 + ")");
            hero.setReso("4");
            return;
        }

    }

    private void build() {
        WebDriver driver = Driver.getInstance();
        WebElement element = driver.findElement(By.className("contractLink"));
        element = element.findElement(By.tagName("button"));
        element.click();
        System.out.println("Заказана постройка уровня");
        element = driver.findElement(By.id("n1"));
        element.click();
    }
}