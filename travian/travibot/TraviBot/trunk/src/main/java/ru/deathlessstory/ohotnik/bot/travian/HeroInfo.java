package ru.deathlessstory.ohotnik.bot.travian;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 07.12.13
 * Time: 12:43
 */
public class HeroInfo {

    private static Hero instance;

    public static Hero getHero() {
        if (instance == null) {
            instance = readHeroStats();
        }
        return instance;
    }

    public static Hero readHeroStats() {
        System.out.println("Считывание характеристик героя");
        Hero h = new Hero();
        WebDriver d = Driver.getInstance();
        WebElement element = d.findElement(By.id("heroImageButton"));
        element.click();
        WebElement sidebar = d.findElement(By.id("sidebarBoxHero"));
        element = sidebar.findElement(By.className("playerName"));
        String name = element.getText().trim();

        h.setName(name);

        System.out.println("Имя героя - " + name);
        return h;
    }

}
