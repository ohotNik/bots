package ru.deathlessstory.ohotnik.bot.travian;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 07.12.13
 * Time: 12:29
 */
public class Driver {

    private static WebDriver driver = new FirefoxDriver();

    public static WebDriver getInstance() {
        return driver;
    }

    private Driver() {
    }

}
